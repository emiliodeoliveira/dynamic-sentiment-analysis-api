import nltk
import pandas as pd
import vec as vec
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB


def dataPreparation(data):
    nltk.download('stopwords')
    nltk.download('punkt')
    nltk.download('wordnet')
    nltk.download('omw-1.4')
    wordLemma = nltk.WordNetLemmatizer()
    stopWords = nltk.corpus.stopwords.words('english')
    wordTokens = nltk.word_tokenize(data)
    lowerText = [word.lower() for word in wordTokens]
    cleanedStopwords = [word for word in lowerText if word not in stopWords]
    noAlpha = [word for word in cleanedStopwords if word.isalpha()]
    lemmaText = [wordLemma.lemmatize(word) for word in noAlpha]
    cleanDataset = lemmaText
    return cleanDataset


def insertRow(df, row):
    insert_loc = df.index.max()

    if pd.isna(insert_loc):
        df.loc[0] = row
    else:
        df.loc[insert_loc + 1] = row


def checkSentimentValue(value):
    if value == 0:
        return "Negative"
    if value == 1:
        return "Positive"


class Analyser:
    reviewDataset = pd.read_csv('datasets/review_dataset.csv')
    trainDataset = pd.read_csv('datasets/train_dataset.csv')
    resultList = pd.DataFrame(columns=['text', 'label'])
    x = trainDataset['text']
    y = trainDataset['label']
    x, x_test, y, y_test = train_test_split(x, y, stratify=y, test_size=0.25, random_state=42)

    vector = CountVectorizer(stop_words='english')
    x = vector.fit_transform(x).toarray()
    x_test = vector.transform(x_test).toarray()

    analysisModel = MultinomialNB()
    analysisModel.fit(x, y)
    for i, j in reviewDataset.iterrows():
        row = str(i)
        cleanText = dataPreparation(row)
        predictResult = analysisModel.predict(vector.transform(cleanText))
        sentimentOutput = checkSentimentValue(predictResult[0])
        insertRow(resultList, [row, sentimentOutput])
    accuracyLevel = analysisModel.score(x_test, y_test)
    resultList.to_csv('analysis_output.csv')
    print("Dataset successfully analysed with " + str(accuracyLevel) + " of accuracy!")

