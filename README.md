# Dynamic Sentiment Analysis API

A dynamic API using Naive Bayes and Random Forest for sentiment analysis.


### Please install the following libraries:

pip install pandas<br>
pip install contractions<br>
pip install nltk<br>
pip install autocorrect<br> 
pip install -U scikit-learn<br>